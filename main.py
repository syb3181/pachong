from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep
from selenium.webdriver import ChromeOptions
import atexit
import argparse
from pymongo import MongoClient
from mongodb_writer import MongodbWriter
from selenium.webdriver.common.keys import Keys

parser = argparse.ArgumentParser()
parser.add_argument('--mongodb_url', default='localhost')
parser.add_argument('--mongodb_port', type=int, default=27017)
parser.add_argument('--pages_to_crawl', type=int, default=5)
parser.add_argument('--pages_to_start', type=int, default=0)
parser.add_argument('--mongodb_user', default='dv')
parser.add_argument('--mongodb_password', default='dv')
parser.add_argument('--db', default='securities')
parser.add_argument('--collection', default='securities')
parser.add_argument('--start_url', default='http://gs.amac.org.cn/amac-infodisc/res/pof/securities/index.html')
parser.add_argument('--detail_url_prefix', default='http://gs.amac.org.cn/amac-infodisc/res/pof/securities')


def get_mongodb_client(host='localhost', port=27017, username='root', password='example'):
    return MongoClient(host=host, port=port, username=username, password=password)


class SecurityInfoCrawler:

    def set_start_url(self, url):
        self.start_url = url

    sleep_before_action = 1

    def __init__(self, driver: webdriver):
        self.start_url = None
        self.driver = driver
        self.url = ''
        self.links = []

    def get_links(self):
        return self.links

    def safe_get(self, url):
        driver = self.driver
        driver.get(url)
        # to handle exception, manually in non-headless mode
        # while '我们只是确认一下你不是机器人' in driver.page_source:
        #     sleep(20)
        #     driver.get(url)

    def extract_security_detail_link(self, page_source):
        try:
            soup = BeautifulSoup(page_source, 'lxml')
            table_body = soup.find(name='tbody')
            a_s = table_body.find_all(name='a', attrs={'class': 'ajaxify'})
            for a in a_s:
                self.links.append(a['href'])
        except Exception as e:
            print(e)

    def crawl_securites(self, params=None):
        driver = self.driver
        self.safe_get(self.start_url)
        page_tot = params['page_tot']
        goto_box = driver.find_element_by_id('goInput')
        goto_box.send_keys(str(params['page_start'] + 1))
        goto_box.send_keys(Keys.ENTER)
        page_count = 0
        while page_count < page_tot:
            try:
                a = driver.find_element_by_id('dvccFundList')
                s = str(a.get_attribute('innerHTML'))
                self.extract_security_detail_link(s)
                paginate = driver.find_element_by_id('dvccFundList_paginate')
                next_button = paginate.find_element_by_class_name('next')
                '''
                something is hovering over the botton which make it not clickable if clicking directly
                '''
                driver.execute_script("arguments[0].click();", next_button)
                sleep(self.sleep_before_action)
                print('next clicked for page {}'.format(params['page_start'] + page_count))
                page_count += 1
            except Exception as e:
                print(e)
                break

    def crawl_detail_page(self, url):
        try:
            driver = self.driver
            driver.get(url)
            sleep(self.sleep_before_action)
            a = driver.find_element_by_class_name('table-response')
            s = str(a.get_attribute('innerHTML'))
            soup = BeautifulSoup(s, 'lxml')
            table_body = soup.find(name='tbody')
            trs = table_body.find_all(name='tr')
            ret = {'url': url}
            for tr in trs:
                tds = tr.find_all(name='td')
                if len(tds) < 2 or tds[0].text == '':
                    continue
                ret[tds[0].text] = tds[1].text
            print(ret)
            writer.write_map(ret)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    ns = parser.parse_args()
    # int mongodb writer
    client = get_mongodb_client(ns.mongodb_url, ns.mongodb_port, ns.mongodb_user, ns.mongodb_password)
    db = client.get_database(ns.db)
    col = db.get_collection(ns.collection)
    writer = MongodbWriter(col, pkey='url')
    # init Chrome driver
    executable_path = r'/Users/yibinsheng/Downloads/chrome_driver/chromedriver'
    options = ChromeOptions()
    # options.add_argument('--headless')
    current_driver = webdriver.Chrome(options=options, executable_path=executable_path)
    atexit.register(lambda: current_driver.close())
    # init crawler
    crawler = SecurityInfoCrawler(current_driver)
    crawler.set_start_url(ns.start_url)
    params = {
        'page_tot': ns.pages_to_crawl,
        'page_start': ns.pages_to_start
    }
    crawler.crawl_securites(params=params)
    for link in crawler.get_links():
        url = '{}/{}'.format(ns.detail_url_prefix, link)
        print(url)
        crawler.crawl_detail_page(url)
    # crawler.crawl_detail_page('http://gs.amac.org.cn/amac-infodisc/res/pof/securities/detail.html?id=2007061221101666')
