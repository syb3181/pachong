from pymongo.database import Collection
from pymongo.errors import DuplicateKeyError


class MongodbWriter:

    def __init__(self, col: Collection, pkey='url'):
        self.col = col
        self.pkey = pkey

    def update_map(self, data: dict, key='url'):
        ori_item = self.col.find_one({key: data[key]})
        if ori_item == data:
            return
        try:
            self.col.remove({key: data[key]})
            self.col.insert_one(data)
        except Exception as e:
            print(e)
            pass

    def write_map(self, data: dict):
        if self.pkey == 'all':
            cnt = self.col.find(data).count()
            if cnt == 0:
                self.col.insert_one(data)
        else:
            try:
                self.col.insert_one(data)
            except DuplicateKeyError:
                pass
            return

    def write_map_list(self, events):
        for event in events:
            self.write_map(event)
